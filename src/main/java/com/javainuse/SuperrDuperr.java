package com.javainuse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SuperrDuperr {

	public static void main(String[] args) {
		SpringApplication.run(SuperrDuperr.class, args);
	}
}
