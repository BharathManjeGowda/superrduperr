package com.javainuse.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.javainuse.dao.ProjectRepository;
import com.javainuse.exception.ProjectCustomException;
import com.javainuse.model.Project;
import com.javainuse.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ProjectController {

	private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProjectRepository projectRepository;

	public ProjectController(ProjectService projectService, ProjectRepository projectRepository) {
		this.projectService = projectService;
		this.projectRepository = projectRepository;
	}

	@ResponseBody
	@RequestMapping(value = "/project", method = RequestMethod.POST)
	public Long newProject(@RequestBody Project project) {
		LOG.debug("Creating new Project.");
		Long projectId = projectService.saveProject(project);
		return projectId;
	}

	@ResponseBody
	@RequestMapping(value = "/projects", method = RequestMethod.POST)
	public List<Long> newProjects(@RequestBody List<Project> projects) {
		List<Long> projectIdList = new ArrayList<>();
		LOG.debug("Creating new Project.");
		for(Project project: projects){
			Long projectId = projectService.saveProject(project);
			projectIdList.add(projectId);
		}
		return projectIdList;
	}

	@ResponseBody
	@RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
	public Project getProjectById(@PathVariable Long id) {
		LOG.debug("Get project for the id:" + id);
		Project project = projectService.findProjectById(id);
		return project;
	}

	@ResponseBody
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public List<Project> getProjectsList() {
		LOG.debug("Get all the projects");
		List<Project> allProjects = projectService.getProjectsList();
		return allProjects;
	}

	@ResponseBody
	@RequestMapping(value = "/project/{id}", method = RequestMethod.PUT)
	public Project updateProject(@PathVariable Long id ,@RequestBody Project project) throws ProjectCustomException {
		LOG.debug("Update project for the id" + id);
		Project updatedProject = projectService.updateProject(id, project);
		return updatedProject;
	}

	@DeleteMapping(value = "/project/{id}")
	public void deleteProjectById(@PathVariable Long id) {
		LOG.debug("Delete project for the id" + id);
		projectRepository.delete(id);
	}

	@ResponseBody
	@RequestMapping(value = "/project/{id}/reminder", method = RequestMethod.PUT)
	public Project updateProject(@PathVariable Long id, @RequestBody Date reminderDate) throws ProjectCustomException {
		LOG.debug("Update reminder for project id" + id);
		Project project = projectService.findProjectById(id);
		project.setReminderDate(reminderDate);
		projectService.updateProject(id, project);
		return project;
	}

}
