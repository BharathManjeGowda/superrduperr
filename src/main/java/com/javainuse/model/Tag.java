package com.javainuse.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Tag implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

   /* @ManyToOne
    @JoinColumn(name="project_id", nullable=false)
    private Project project;*/

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
