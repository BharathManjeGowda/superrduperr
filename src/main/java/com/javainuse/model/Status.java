package com.javainuse.model;

public enum Status {

    RAISED,
    COMPLETED
}
