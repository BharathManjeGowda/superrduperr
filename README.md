How to run project:-

1. Download the project.
2. Open in IntelIj.
3. Maven clean install.
4. Run SuperrDuperr.java.

_____________________________________________________________________________

REST end points:-

1. Save project: POST : http://localhost:8080/project

BODY: {
         "name":"Test project 1",
         "status":"completed",
         "tags":[
            {
               "name":"name1"
            }
         ],
         "reminderDate":"2012-01-30 09:00:00"
      }
_____________________________________________________________________________

2. Get Project by ID - GET - http://localhost:8080/project/1

_____________________________________________________________________________

3. Get all Projects - GET - http://localhost:8080/projects

_____________________________________________________________________________

4. Update a Project - PUT - http://localhost:8080/project/1

BODY: {
         "name":"Test project 1",
         "status":"completed",
         "tags":[
            {
               "name":"name1"
            }
         ],
         "reminderDate":"2012-01-30 09:00:00"
      }
_____________________________________________________________________________

5. Delete a Project - DELETE - http://localhost:8080/project/1

_____________________________________________________________________________

6. Set project reminder - PUT - http://localhost:8080/project/1/reminder

BODY: "2012-01-30 09:33:00"

_____________________________________________________________________________

7. Save a project list - POST - http://localhost:8080/projects

BODY: [
         {
            "name":"Test project 5",
            "status":"completed",
            "tags":[
               {
                  "name":"name1"
               }
            ],
            "reminderDate":"2012-01-30 09:00:00"
         },
         {
            "name":"Test project 6",
            "status":"completed",
            "tags":[
               {
                  "name":"name1"
               }
            ],
            "reminderDate":"2012-01-30 09:00:00"
         }
      ]

_____________________________________________________________________________

